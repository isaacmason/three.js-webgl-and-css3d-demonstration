var controls, camera, scene, webGLRenderer, css3DRenderer, light, cubeMesh, domElement;
init();
animate();

function init() {
    // Create the scene
    scene = new THREE.Scene();

    // Create the camera
    camera = createCamera();

    // Create the light
    light = createLight();
    scene.add( light );

    // Create the cube mesh
    cubeMesh = createCube();
    scene.add( cubeMesh );

    // Create the DOM element
    domElement = createText();
    scene.add( domElement )

    // Create the renderers
    webGLRenderer = createWebGLRenderer();
    css3DRenderer = createCSS3DRenderer();

    // Create the controls
    controls = createControls();

    // Create the event listener for on resize
    window.addEventListener( 'resize', onWindowResize, false );
    onWindowResize();
}

function createCamera() {
    var camera = new THREE.PerspectiveCamera( 70, 1, 1, 1000 );
    camera.position.z = 500;
    return camera;
}

function createLight() {
    var lights = new THREE.Group();

    var directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
    directionalLight.position = new THREE.Vector3(  300, 0, 300 );
    directionalLight.lookAt( new THREE.Vector3( 0, 0, 0 ) );
    lights.add( directionalLight );

    var ambientLight = new THREE.AmbientLight( 0xffffff, .5 );
    ambientLight.position = new THREE.Vector3(  0, -200, 400 );
    ambientLight.lookAt( new THREE.Vector3( 0, 0, 0 ) );
    lights.add( ambientLight );

    return lights;
}

function createCube() {
    var geometry = new THREE.BoxGeometry( 50, 50, 50 );
    var material = new THREE.MeshPhongMaterial( {
        color: 0x002d77,
        specular: 0x111111,
        shininess: 30,
    } );
    var cube = new THREE.Mesh( geometry, material );
    return cube;
}

function createWebGLRenderer() {
    var rendererContainer = document.querySelector("#webgl");
    var renderer = new THREE.WebGLRenderer( { 
        antialias: true,
    } );
    renderer.setPixelRatio( window.devicePixelRatio );
    rendererContainer.prepend( renderer.domElement );
    renderer.setSize( window.innerWidth, window.innerHeight );
    return renderer;
}

function createText() {
    var element = document.createElement( 'div' );
    element.innerHTML = "<p style='color: white;'>Drag me around!</p>"
    var domObject = new THREE.CSS3DObject( element );
    domObject.position.z = 50;
    return domObject;
}

function createCSS3DRenderer() {
    var rendererContainer = document.querySelector("#css");
    var renderer = new THREE.CSS3DRenderer();
    rendererContainer.prepend( renderer.domElement );
    renderer.setSize( window.innerWidth, window.innerHeight );
    return renderer;
}

function createControls() {
    var controls = new THREE.OrbitControls( camera, css3DRenderer.domElement );
    return controls;
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    css3DRenderer.setSize( window.innerWidth, window.innerHeight );
    webGLRenderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
    requestAnimationFrame( animate );
    controls.update();
    cubeMesh.rotation.x += 0.015;
    cubeMesh.rotation.y += 0.02;
    webGLRenderer.render( scene, camera );
    css3DRenderer.render( scene, camera );
}
